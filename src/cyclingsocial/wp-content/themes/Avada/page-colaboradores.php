<?php
/* Template Name: Formulario Colaboradores */
?>
<?php
/**
 * Template used for pages.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
    <h1 style="text-align: center; font-size: 75px;">Añade tu Club</h1>
    <form name="contenidoColaboradores" id="contenidoColaboradores" method="post">
        <p>
            <?php if(get_current_user_id()!=0) echo'<input type="text" name="tituloPost" placeholder="Escribe aquí el título del Club"/>';?>
        </p>

        <p>
            <?php
                if(get_current_user_id()!=0){
                    wp_editor(
                        $post_obj->post_content,
                        'userpostcontent',
                        array('textarea_name' => 'contenido')
                    );
                }else{
                    echo'<h1>No tienes acceso a esta información. Para iniciar sesión <a href="16.2daw.esvirgua.com/login">Haz click aquí</a></h1>';
                }
            ?>
        </p>

        <p>
            <?php if(get_current_user_id()!=0) echo' <input type="submit" class="wpem-theme-button" name="enviar" value="AÑADIR CLUB"/>';?>
        </p>

        <?php wp_nonce_field('contenidoColaboradores'); ?>
    </form>
</section>
<?php
    if(isset($_POST["enviar"])){
        if(get_current_user_id()==0){
            echo'<h3 style="color: darkred;">Para poder realizar el alta de un club tiene que iniciar sesión. <a href="16.2daw.esvirgua.com/login">Haz click aquí</a>.</h3>';
        }else{
            if(empty($_POST["tituloPost"]) || empty($_POST["contenido"])){
                echo'<h3 style="color: darkred;">Debes de rellenar todos los campos</h3>';
            }else{
                if(get_current_user_id()==1){
                    $mi_entrada = array(
                        'post_title' => wp_strip_all_tags($_POST['tituloPost']),
                        'post_content' => $_POST['contenido'],
                        'post_type' =>'post',
                        'post_status' => 'Publish',
                        'post_author' => get_current_user_id(),
                    );
                    $nuevo_post = wp_insert_post($mi_entrada);
                    echo'<h3 style="color: darkgreen;">Club <span style="color: darkslategrey">'.$_POST["tituloPost"].'</span> creado con éxito.</h3>';
                }else{
                    $conect= new mysqli('16.2daw.esvirgua.com','user2daw_16','^#bIt[eL+lhS','user2daw_BD1-16');

                    $masdeunclub="SELECT post_title FROM wp_posts WHERE post_author=".get_current_user_id()." and post_type='post';";

                    $resultado=$conect->query($masdeunclub);

                    if($resultado->num_rows<1){
                        $mi_entrada = array(
                            'post_title' => wp_strip_all_tags($_POST['tituloPost']),
                            'post_content' => $_POST['contenido'],
                            'post_type' =>'post',
                            'post_status' => 'publish',
                            'post_author' => get_current_user_id(),
                        );
                        $nuevo_post = wp_insert_post($mi_entrada);
                        echo'<h3 style="color: darkgreen;">Club <span style="color: darkslategrey">'.$_POST["tituloPost"].'</span> creado con éxito.</h3>';
                    }else{
                        if($fila=$resultado->fetch_array()){
                            echo'<h3>Ya ha creado un club, es <span style="color: darkslategrey">'.$fila["post_title"].'</span> si desea añadir uno debe borrar el actual</h3>';
                        }
                    }
                }
            }
        }
    }
?>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>
