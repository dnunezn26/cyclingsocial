<?php
/* Template Name:VALORACION */
?>
<?php
/**
 * Template used for pages.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php echo fusion_render_rich_snippets_for_pages(); // phpcs:ignore WordPress.Security.EscapeOutput ?>

            <?php avada_singular_featured_image(); ?>

            <div class="post-content">
                <?php the_content(); ?>
                <?php fusion_link_pages(); ?>
            </div>
            <?php if ( ! post_password_required( $post->ID ) ) : ?>
                <?php do_action( 'avada_before_additional_page_content' ); ?>
                <?php if ( class_exists( 'WooCommerce' ) ) : ?>
                    <?php $woo_thanks_page_id = get_option( 'woocommerce_thanks_page_id' ); ?>
                    <?php $is_woo_thanks_page = ( ! get_option( 'woocommerce_thanks_page_id' ) ) ? false : is_page( get_option( 'woocommerce_thanks_page_id' ) ); ?>
                    <?php if ( Avada()->settings->get( 'comments_pages' ) && ! is_cart() && ! is_checkout() && ! is_account_page() && ! $is_woo_thanks_page ) : ?>
                        <?php comments_template(); ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
                        <?php comments_template(); ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php do_action( 'avada_after_additional_page_content' ); ?>
            <?php endif; // Password check. ?>
        </div>
    <?php endwhile; ?>
</section>
<?php
    $conect= new mysqli('16.2daw.esvirgua.com','user2daw_16','^#bIt[eL+lhS','user2daw_BD1-16');//crear conexión
    if (isset($_POST["valorar"])){
        $valoracion="UPDATE wp_registration SET valoracion=".$_POST["estrellas"]." WHERE ID_posts=".$_POST["club"]." and ID_users=".get_current_user_id().";";
        $conect->query($valoracion);
        header("Location: 16.2daw.esvirgua.com/participaciones");
    }else{
        if(get_current_user_id()==0){
            echo'<h1>No tiene acceso a esta información. Tiene que iniciar sesión. <a href="16.2daw.esvirgua.com/login">Haz click aquí</a>.</h1>';
        }else{
            if(isset($_GET["q"])){


                $nombreevento="SELECT post_title FROM wp_posts WHERE ID=".$_GET["q"].";";

                $resultado=$conect->query($nombreevento);

                if($fila=$resultado->fetch_array()){
                    echo'<h1 style="font-size: 70px">Valore <span style="color: darkslategrey">'.$fila["post_title"].'</span></h1>';
                }
                echo'<div style="margin: 0 auto;"><form style="text-align: center;" id="formu" method="post">
                  <p class="clasificacion">
                    <input type="hidden" name="club" value="'.$_GET["q"].'"/>
                    <input id="radio1" type="radio" name="estrellas" value="5">
                    <label for="radio1">&#9733;</label>
                    <input id="radio2" type="radio" name="estrellas" value="4">
                    <label for="radio2">&#9733;</label>
                    <input id="radio3" type="radio" name="estrellas" value="3">
                    <label for="radio3">&#9733;</label>
                    <input id="radio4" type="radio" name="estrellas" value="2">
                    <label for="radio4">&#9733;</label>
                    <input id="radio5" type="radio" name="estrellas" value="1" checked>
                    <label for="radio5">&#9733;</label>
                  </p>
                  <input style="margin: 0 auto; width: 150px;" type="submit" class="wpem-theme-button" name="valorar" value="VALORAR"/>
                </form></div>';
            }else{
                echo'<h1>No tiene acceso a esta información.</h1>';
            }
        }
    }

?>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>
