<?php
/* Template Name:PARTICIPACIONES */
?>
<?php
/**
 * Template used for pages.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
    <?php while ( have_posts() ) : ?>
        <?php the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php echo fusion_render_rich_snippets_for_pages(); // phpcs:ignore WordPress.Security.EscapeOutput ?>

            <?php avada_singular_featured_image(); ?>

            <div class="post-content">
                <?php the_content(); ?>
                <?php fusion_link_pages(); ?>
            </div>
            <?php if ( ! post_password_required( $post->ID ) ) : ?>
                <?php do_action( 'avada_before_additional_page_content' ); ?>
                <?php if ( class_exists( 'WooCommerce' ) ) : ?>
                    <?php $woo_thanks_page_id = get_option( 'woocommerce_thanks_page_id' ); ?>
                    <?php $is_woo_thanks_page = ( ! get_option( 'woocommerce_thanks_page_id' ) ) ? false : is_page( get_option( 'woocommerce_thanks_page_id' ) ); ?>
                    <?php if ( Avada()->settings->get( 'comments_pages' ) && ! is_cart() && ! is_checkout() && ! is_account_page() && ! $is_woo_thanks_page ) : ?>
                        <?php comments_template(); ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
                        <?php comments_template(); ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php do_action( 'avada_after_additional_page_content' ); ?>
            <?php endif; // Password check. ?>
        </div>
    <?php endwhile; ?>
</section>
<?php
    if(get_current_user_id()==0){
        echo'<h1>No tiene acceso a esta información. Tiene que iniciar sesión. <a href="16.2daw.esvirgua.com/login">Haz click aquí</a>.</h1>';
    }else{
        $conect= new mysqli('16.2daw.esvirgua.com','user2daw_16','^#bIt[eL+lhS','user2daw_BD1-16');//crear conexión

        $nombreciclista="SELECT display_name FROM wp_users WHERE ID=".get_current_user_id().";";

        $resultado=$conect->query($nombreciclista);

        if($fila=$resultado->fetch_array()){
            echo '<h1>Hola <span style="color: darkslategrey">'.$fila["display_name"].'</span>, estos son tu eventos</h1>';
        }
        $participaciones="SELECT ID, post_title, wp.meta_value FROM wp_posts wp2 INNER JOIN wp_postmeta wp ON wp2.ID=wp.post_id INNER JOIN wp_registration wr ON wp2.ID = wr.ID_posts WHERE wr.ID_users =".get_current_user_id()." and wp.meta_key='_event_start_date';";

        $resultado2=$conect->query($participaciones);
        echo '<table style="margin: 0 auto; text-align: center" width="80%" border="1">';//visualizamos los eventos en los que se ha registrado en una tabla
        echo '<tr>
                            <th><h1>Nombre Evento</h1></th>
                            <th><h1>Fecha Inicio</h1></th>
                            <th><h1>¿VALORAR?</h1></th>
                          </tr>';
        while ($filas=$resultado2->fetch_array()){
            $pregunta="SELECT valoracion FROM wp_registration WHERE ID_users=".get_current_user_id()." and ID_posts=".$filas["ID"].";";
            $resultado3=$conect->query($pregunta);
            echo utf8_encode('<tr>
                                <td><h3>'.$filas['post_title'].'</h3></td>
                                <td><h3>'.$filas['meta_value'].'</h3></td>');
                                if($fila3=$resultado3->fetch_array()){
                                    if (is_null($fila3["valoracion"])){
                                        echo' <td><h3><a href="16.2daw.esvirgua.com/valorar?q='.$filas["ID"].'">VALORAR</a></h3></td>';
                                    }else{
                                        switch ($fila3["valoracion"]){
                                            case 1:
                                                echo'<td><label style="color: orange">&#9733;</label> <label>&#9733;</label> <label>&#9733;</label> <label>&#9733;</label> <label>&#9733;</label></td>';
                                                break;
                                            case 2:
                                                echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label>&#9733;</label> <label>&#9733;</label> <label>&#9733;</label></td>';
                                                break;
                                            case 3:
                                                echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label>&#9733;</label> <label>&#9733;</label></td>';
                                                break;
                                            case 4:
                                                echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label>&#9733;</label></td>';
                                                break;
                                            case 5:
                                                echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label></td>';
                                                break;
                                        }
                                    }
                                }
                              echo'</tr>';
        }
        if($resultado2->num_rows==0){//si no hay filas tras ejecutar la consulta nos mostrará un mensaje diciendo que no hay inscritos
            echo '<tr><td colspan="3"><h3>No ha ciclistas inscritos en tu club</h3></td></tr>';
        }
        echo '</table>';
    }
    ?>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>
