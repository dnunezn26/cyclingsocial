<?php
/* Template Name: LISTADO PARTICIPANTES */
?>
<?php
/**
 * Template used for pages.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
    exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
    <section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php echo fusion_render_rich_snippets_for_pages(); // phpcs:ignore WordPress.Security.EscapeOutput ?>

        <?php avada_singular_featured_image(); ?>

        <div class="post-content">
            <?php the_content(); ?>
            <?php fusion_link_pages(); ?>
        </div>
        <?php if ( ! post_password_required( $post->ID ) ) : ?>
            <?php do_action( 'avada_before_additional_page_content' ); ?>
            <?php if ( class_exists( 'WooCommerce' ) ) : ?>
                <?php $woo_thanks_page_id = get_option( 'woocommerce_thanks_page_id' ); ?>
                <?php $is_woo_thanks_page = ( ! get_option( 'woocommerce_thanks_page_id' ) ) ? false : is_page( get_option( 'woocommerce_thanks_page_id' ) ); ?>
                <?php if ( Avada()->settings->get( 'comments_pages' ) && ! is_cart() && ! is_checkout() && ! is_account_page() && ! $is_woo_thanks_page ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
            <?php else : ?>
                <?php if ( Avada()->settings->get( 'comments_pages' ) ) : ?>
                    <?php comments_template(); ?>
                <?php endif; ?>
            <?php endif; ?>
            <?php do_action( 'avada_after_additional_page_content' ); ?>
        <?php endif; // Password check. ?>
    </div>
    <?php
        if(!isset($_POST['id_evento'])){
            echo '<h1>No tiene acceso a esta información</h1>';
        }else{
            $conect= new mysqli('16.2daw.esvirgua.com','user2daw_16','^#bIt[eL+lhS','user2daw_BD1-16');


            $title_event="SELECT post_title, meta_value FROM  wp_posts inner join wp_postmeta on ID=post_id  where ID=".$_POST['id_evento']." and meta_key='_distancia_km';";

            $resultado=$conect->query( $title_event);
            if($fila=$resultado->fetch_array()){
                echo '<h1 style="font-size: 75px;">'.$fila['post_title'].' <span style="font-size: 60px; color: darkslategrey;">'.$fila['meta_value'].'</span><span style="font-size: 25px"><a href="javascript: history.go(-1)">  Volver</a></span></h1>';
            }

            $eventos="SELECT display_name, user_email, user_registered, valoracion FROM wp_users INNER JOIN wp_registration ON ID=ID_users WHERE ID_posts=".$_POST['id_evento'].";";

            $resultado2=$conect->query($eventos);



            echo '<table style="margin: 0 auto; text-align: center" width="80%" border="1">';
            echo '<tr>
                    <th><h1>Nombre</h1></th>
                    <th><h1>Email</h1></th>
                    <th><h1>Fecha Registro</h1></th>
                    <th><h1>Valoracion</h1></th>
                  </tr>';
            while ($filas=$resultado2->fetch_array()){
                echo utf8_encode('<tr>
                        <td><h3>'.$filas['display_name'].'</h3></td>
                        <td><h3><a href="mailto:'.$filas['user_email'].'">'.$filas['user_email'].'</a></h3></td>
                        <td><h3>'.$filas['user_registered'].'</h3></td>');
                        if (is_null($filas["valoracion"])){
                            echo' <td><h3>SIN VALORAR</h3></td>';
                        }else{
                            switch ($filas["valoracion"]){
                                case 1:
                                    echo'<td><label style="color: orange">&#9733;</label> <label>&#9733;</label> <label>&#9733;</label> <label>&#9733;</label> <label>&#9733;</label></td>';
                                    break;
                                case 2:
                                    echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label>&#9733;</label> <label>&#9733;</label> <label>&#9733;</label></td>';
                                    break;
                                case 3:
                                    echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label>&#9733;</label> <label>&#9733;</label></td>';
                                    break;
                                case 4:
                                    echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label>&#9733;</label></td>';
                                    break;
                                case 5:
                                    echo'<td><label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label> <label style="color: orange">&#9733;</label></td>';
                                    break;
                            }
                        }
                        echo '</tr>';
            }
            echo '</table>';
        }
    ?>
    </section>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>