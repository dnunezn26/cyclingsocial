<?php
/**
 * Template used for single posts and other post-types
 * that don't have a specific template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>

<?php get_header(); ?>

<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>
	<?php if ( fusion_get_option( 'blog_pn_nav' ) ) : ?>
		<div class="single-navigation clearfix">
			<?php previous_post_link( '%link', esc_attr__( 'Previous', 'Avada' ) ); ?>
			<?php next_post_link( '%link', esc_attr__( 'Next', 'Avada' ) ); ?>
		</div>
	<?php endif; ?>

	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
			<?php $full_image = ''; ?>
            <!--Código añadido por Darío Núñez-->
            <div style="margin: 0 auto; text-align: center;">
            <?php
            $post=get_post();//Esta función obtendra un array con todos los datos del club en el que estamos ahora mismo, la meto en una variable
                if($post->post_type=='post') {
                    if (isset($_POST["eliminar"])) {//Pregunto si existe la variable $post eliminar que se creara cuando envie el formulario
                        $conect = new mysqli('16.2daw.esvirgua.com', 'user2daw_16', '^#bIt[eL+lhS', 'user2daw_BD1-16');//crear conexion
                        $borrar = "DELETE FROM wp_inscriptions WHERE ID_users=" . get_current_user_id() . ";";//Eliminamos inscripción a club usuario que tiene la sesión inicia, es decir, el que ha pulsado el boton.
                        $conect->query($borrar);
                        if ($conect->affected_rows != -1) {
                            echo '<h1>Se ha eliminado su inscripción con este Club</h1>';
                        }
                        $conect->close();
                    }
                    if (isset($_POST["inscribir"])) {//Pregunto si existe la variable $post incribir que se creara cuando envie el formulario
                        $conect = new mysqli('16.2daw.esvirgua.com', 'user2daw_16', '^#bIt[eL+lhS', 'user2daw_BD1-16');
                        $alta = "INSERT INTO wp_inscriptions VALUES (" . get_current_user_id() . "," . $post->ID . ");";//Insertamos un nuevo inscrito en la tabla previamente creada, guardando el id que tiene la sesión iniciada y el id del club en el que nos encontramos
                        $conect->query($alta);
                        $conect->close();//cerrar conexión
                    }
                }
            ?>
            <?php
                $conect= new mysqli('16.2daw.esvirgua.com','user2daw_16','^#bIt[eL+lhS','user2daw_BD1-16');//nueva conexión
                if($post->post_type=='post'){//Como solo quiero que se pueda realizar el proceso de inscripciones solo en clubes(ENTRADAS) pregunto primero si el post en el que nos encontramos es tipo post, es decir, tipo entrada
                    if(get_current_user_id()==0){//si el id del usuario es 0, significa que no ha iniciado sesión, con lo cual no tiene acceso a la web
                        echo '<h1>Si desea inscribirse a este club debe iniciar sesión. <a href="16.2daw.esvirgua.com/login">Haz clik aqui</a></h1>';
                    }else{
                        $id_club=$post->ID;//meto en variable el id del club en el que nos encontramos
                        $id_author_club=$post->post_author;//meto en variable el id del creador del club
                        if(get_current_user_id()==$id_author_club){//si el id del usuario que esta iniciado es igual al id del creador del club, nos mostrará un botón para ver los integrantes del club
                            echo '<form action="../../../../integrantes" method="post">  
                                            <input type="hidden" name="id_club" value="'.$id_club.'"/>
                                            <input type="submit" class="wpem-theme-button" value="VER INTEGRANTES DEL CLUB"/>
                                      </form>';//mandamos de forma oculta el id del club en el que estamos para poder realizar la consulta desde la página a la que le mandamos la variable
                        }else{//si no eres el creador del club
                            $consulta_info="SELECT * FROM wp_inscriptions WHERE ID_users=".get_current_user_id().";";//consulta para saber si el usuario esta inscrito en algun club

                            $resultado=$conect->query($consulta_info);

                            if($resultado->num_rows==0){//si no devuelve ninguna fila significa que no estamos inscritos en ningún club y nos monstrará botón para realizar inscripción
                                echo '<form style="margin: 0 auto;" action="#" method="post">
                                                  <input type="submit" name="inscribir" class="wpem-theme-button" value="INSCRIBIRME A ESTE CLUB">
                                              </form>';
                            }else{//en el caso de que nos devuelva alguna fila, significa que el usuario esta subscrito en algún club
                                $consulta_info2="SELECT post_title, ID_users, ID_posts FROM wp_inscriptions INNER JOIN wp_posts ON ID_posts=ID WHERE ID_users=".get_current_user_id()." AND ID=".$id_club.";";//consulta para saber si usuario esta inscrito en el club que nos encontramos
                                $resultado2=$conect->query($consulta_info2);

                                if($resultado2->num_rows>0){//Si nos devuelve filas nos informa que estamos inscritos y nos da opción a eliminar la inscripción al club
                                    echo '<h1>Ya esta inscrito en este club</h1>
                                              <form action="#" method="post">
                                                  <input type="submit" name="eliminar" class="wpem-theme-button" value="ELIMINAR INSCRIPCIÓN A ESTE CLUB">
                                              </form>';
                                }else{//en el caso que nos devuelva filas, significa que esta inscrito pero en otro club distinto al que estamos
                                    if($fila=$resultado->fetch_array()){
                                        $consulta_info3="SELECT post_title, ID_users, ID_posts FROM wp_inscriptions INNER JOIN wp_posts ON ID_posts=ID WHERE ID_users=".get_current_user_id()." AND ID=".$fila['ID_posts'].";";//consulta para sacar en que club estamos inscritos
                                        $resultado3=$conect->query($consulta_info3);
                                        if($fila2=$resultado3->fetch_array()){
                                            echo'<h1>Esta inscrito en el Club, '.$fila2["post_title"].'</h1>';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            ?>
            </div>
			<?php if ( 'above' === Avada()->settings->get( 'blog_post_title' ) ) : ?>
				<?php if ( 'below_title' === Avada()->settings->get( 'blog_post_meta_position' ) ) : ?>
					<div class="fusion-post-title-meta-wrap">
				<?php endif; ?>
				<?php $title_size = ( false === avada_is_page_title_bar_enabled( $post->ID ) ? '1' : '2' ); ?>
				<?php echo avada_render_post_title( $post->ID, false, '', $title_size ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
				<?php if ( 'below_title' === Avada()->settings->get( 'blog_post_meta_position' ) ) : ?>
					<?php echo avada_render_post_metadata( 'single' ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
					</div>
				<?php endif; ?>
			<?php elseif ( 'disabled' === Avada()->settings->get( 'blog_post_title' ) && Avada()->settings->get( 'disable_date_rich_snippet_pages' ) && Avada()->settings->get( 'disable_rich_snippet_title' ) ) : ?>
				<span class="entry-title" style="display: none;"><?php the_title(); ?></span>
			<?php endif; ?>

			<?php avada_singular_featured_image(); ?>

			<?php if ( 'below' === Avada()->settings->get( 'blog_post_title' ) ) : ?>
				<?php if ( 'below_title' === Avada()->settings->get( 'blog_post_meta_position' ) ) : ?>
					<div class="fusion-post-title-meta-wrap">
				<?php endif; ?>
				<?php $title_size = ( false === avada_is_page_title_bar_enabled( $post->ID ) ? '1' : '2' ); ?>
				<?php echo avada_render_post_title( $post->ID, false, '', $title_size ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
				<?php if ( 'below_title' === Avada()->settings->get( 'blog_post_meta_position' ) ) : ?>
					<?php echo avada_render_post_metadata( 'single' ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<div class="post-content">
				<?php the_content(); ?>
				<?php fusion_link_pages(); ?>
			</div>

			<?php if ( ! post_password_required( $post->ID ) ) : ?>
				<?php if ( '' === Avada()->settings->get( 'blog_post_meta_position' ) || 'below_article' === Avada()->settings->get( 'blog_post_meta_position' ) || 'disabled' === Avada()->settings->get( 'blog_post_title' ) ) : ?>
					<?php echo avada_render_post_metadata( 'single' ); // phpcs:ignore WordPress.Security.EscapeOutput ?>
				<?php endif; ?>
				<?php do_action( 'avada_before_additional_post_content' ); ?>
				<?php avada_render_social_sharing(); ?>
				<?php $author_info = fusion_get_page_option( 'author_info', $post->ID ); ?>
				<?php if ( ( Avada()->settings->get( 'author_info' ) && 'no' !== $author_info ) || ( ! Avada()->settings->get( 'author_info' ) && 'yes' === $author_info ) ) : ?>
					<section class="about-author">
						<?php ob_start(); ?>
						<?php the_author_posts_link(); ?>
						<?php /* translators: The link. */ ?>
						<?php $title = sprintf( __( 'About the Author: %s', 'Avada' ), ob_get_clean() ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride ?>
						<?php $title_size = ( false === avada_is_page_title_bar_enabled( $post->ID ) ? '2' : '3' ); ?>
						<?php Avada()->template->title_template( $title, $title_size ); ?>
						<div class="about-author-container">
							<div class="avatar">
								<?php echo get_avatar( get_the_author_meta( 'email' ), '72' ); ?>
							</div>
							<div class="description">
								<?php the_author_meta( 'description' ); ?>
							</div>
						</div>
					</section>
				<?php endif; ?>
				<?php avada_render_related_posts( get_post_type() ); // Render Related Posts. ?>
                <!--SACAR LISTADO DE INTEGRANTES-->
                <?php
                    if(get_current_user_id()!=0){
                        if($post->post_type=='post') {////Como solo quiero que se pueda realizar el proceso de inscripciones solo en clubes(ENTRADAS) pregunto primero si el post en el que nos encontramos es tipo post, es decir, tipo entrada
                            $integrantes = "SELECT COUNT(*) as integrantes FROM wp_inscriptions WHERE ID_posts=" . $id_club . "; ";//consulta para contar el número de inscritos en el club en el que estamos

                            $resultado = $conect->query($integrantes);

                            if ($fila = $resultado->fetch_array())
                                echo '<h1>Nº INTEGRANTES <span style="color: darkgreen">' . $fila["integrantes"] . '</span></h1>';
                        }
                    }
                ?>
                <!--+++++++++++++++++++++++++++++++-->
				<?php $post_comments = fusion_get_page_option( 'blog_comments', $post->ID ); ?>
				<?php if ( ( Avada()->settings->get( 'blog_comments' ) && 'no' !== $post_comments ) || ( ! Avada()->settings->get( 'blog_comments' ) && 'yes' === $post_comments ) ) : ?>
					<?php comments_template(); ?>
				<?php endif; ?>
				<?php do_action( 'avada_after_additional_post_content' ); ?>
			<?php endif; ?>
		</article>
	<?php endwhile; ?>
</section>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer(); ?>
